Feature: Validate school supervisor login

	As school supervisor
	I must be able to login
	So I can use as

	Scenario: Login to AS with school supervisor role
		When I enter the school supervisor credentials
		Then I should see school supervisor dashboard
