require_relative 'login_helper'

include LoginHelper

When ("I enter the school supervisor credentials") do
  chrome
  @driver.find_element(:name, "email").send_keys("pablol+ci@amco.me")
  @driver.find_element(:name, "password").send_keys("")
  @driver.find_element(:xpath, "//input[@type='submit']").click
end

Then ("I should see school supervisor dashboard") do
  shipit
end
