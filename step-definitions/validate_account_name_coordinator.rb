Given 'I navigate to AS' do
  @as = visit(AmcoId)
  @as
end

When 'I login to Academic Solution as coordinator' do
  @as.enter_user 'coordinator', 'email'
  @as.enter_password 'coordinator', 'pass'
  @as.access_to_as
end

And 'I navigate to settings section' do
  @as.open_settings
end

And 'I change the language' do
  @as = visit(Settings)
  @as.change_language
end

Then 'I should see my account name displayed' do
  @as = visit(Settings)
  @as.validate_account_name
end

Then 'I should see my assigned school' do
  @as = visit(Settings)
  @as.validate_school_name
end
