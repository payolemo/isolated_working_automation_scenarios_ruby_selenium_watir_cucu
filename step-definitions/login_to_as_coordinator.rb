Given 'I\'m on the AS page' do
  @site = visit(AmcoId)
  @site
end

When 'I login with email' do
  @site.enter_user 'coordinator', 'email'
  @site.enter_password 'coordinator', 'pass'
  @site.access_to_as
end

When 'I login with username' do
  @site.enter_user 'coordinator', 'username'
  @site.enter_password 'coordinator', 'pass'
  @site.access_to_as
end

When 'I login with google account' do
  @site.enter_user 'coordinator', 'gsuite'
  @site.enter_password 'coordinator', 'pass'
  @site.access_to_as
end

Then 'I must be on the teachers view' do
  @site.login_text
end
