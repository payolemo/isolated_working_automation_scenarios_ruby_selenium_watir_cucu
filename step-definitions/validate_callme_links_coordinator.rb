Given 'On the AS page' do
  @as = visit(AmcoId)
  @as
end

When 'I login to as' do
  @as.enter_user 'coordinator', 'email'
  @as.enter_password 'coordinator', 'pass'
  @as.access_to_as
end

And 'I navigate to help view' do
  @as.open_help_view
end

And 'I click on callme link' do
  @as = visit(AsHelp)
  @as.navigate_to_callme
end

And 'I must be able to see callme site' do
  @as.validate_callme_url
end
