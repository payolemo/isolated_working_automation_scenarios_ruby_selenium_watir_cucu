Feature: Validate coordinator login

	As coordinator
	I must be able to login
	So I can use the app

  Scenario: Loging in to AS with coordinator role
    When I enter the coordinator credentials
    Then I should see coordinator welcome message
