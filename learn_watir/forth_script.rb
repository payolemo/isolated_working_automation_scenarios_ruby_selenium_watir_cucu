require 'rubygems'
require 'watir-webdriver'
require_relative 'adoption_helper'

include AdoptionHelper

goto_the_puppy_adoption_site
adopt_puppy_number 0
to_make_possible_adopt_another_puppy
adopt_puppy_number 2
choose_additional_products
checkout_with('Pabloski', '123 Main', 'pablol@amco.me', 'Check')
verify_alert 'Thank you for adopting a puppy!'
close_the_browser

