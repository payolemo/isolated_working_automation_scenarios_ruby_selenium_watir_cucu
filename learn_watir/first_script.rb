require 'rubygems'
require 'watir-webdriver'

browser = Watir::Browser.new :chrome
browser.goto ("puppies.herokuapp.com")
sleep 3
browser.button(:value => 'View Details').click
sleep 3
browser.button(:value => 'Adopt Me!').click
sleep 2
browser.checkbox(:id => 'collar').click
browser.checkbox(:id => 'carrier').click
sleep 2
browser.button(:value => 'Complete the Adoption').click
sleep 2
browser.text_field(:id => 'order_name').set 'Pablo Leon Moran'
sleep 2
browser.textarea(:id => 'order_address').set 'Zamora No. 20'
sleep 2
browser.text_field(:id => 'order_email').set 'pablol@amcooonline.net'
sleep 1
browser.select_list(:id => 'order_pay_type').select 'Check'
sleep 1
browser.button(:name => 'commit').click
sleep 4
fail 'Browser text did not match expected value' unless browser.text.include? 'Thank you for adopting a coffe cup!'
browser.close
