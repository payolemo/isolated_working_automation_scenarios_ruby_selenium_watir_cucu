require 'rubygems'
require 'watir-webdriver'

def goto_the_puppy_adoption_site
	@browser = Watir::Browser.new :chrome
	@browser.goto ('puppies.herokuapp.com')
end

def adopt_puppy_number(num)
	@browser.button(:value => 'View Details', :index => num).click
	sleep 2
	@browser.button(:value => 'Adopt Me!').click
	sleep 2
end

def choose_additional_products
	@browser.checkbox(:value => '1',  :index => 0).click
	@browser.checkbox(:value => '1', :index => 2).click
	sleep 1
	@browser.checkbox(:value => '1', :index => 4).click
	@browser.checkbox(:value => '1', :index => 6).click
	sleep 2
	@browser.button(:value => 'Complete the Adoption').click
end

def to_make_possible_adopt_another_puppy                                           
        @browser.button(:value => 'Adopt Another Puppy').click                     
        sleep 2                                                                    
end

def checkout_with(name, address, email, pay_type)
	@browser.textarea(:id => 'order_name').set(name)
	sleep 1
	@browser.textarea(:id => 'order_address').set(address)
	@browser.textarea(:id => 'order_email').set(email)
	sleep 1
	@browser.select_list(:name => 'order[pay_type]').select(pay_type)
	sleep 1
	@browser.button(:value => 'Place Order').click
	sleep 2
end

def verify_alert(text)
fail'Ups! falto mostrar la alerta, hecha un vistaso!!' unless @browser.text.include? text
end

def close_the_browser
	@browser.close
end

goto_the_puppy_adoption_site
adopt_puppy_number 0
to_make_possible_adopt_another_puppy
adopt_puppy_number 2
choose_additional_products
checkout_with('Pablo Leon Moran', 'Zamora No. 20 Col. Ladron de Guevara', 'pablol@amcoonline.net', 'Check')
sleep 1
verify_alert 'Thank you for adopting a puppy!'
close_the_browser
