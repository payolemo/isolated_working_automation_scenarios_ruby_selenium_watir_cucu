require_relative 'login_helper'

include LoginHelper

When ("I enter the Advisor credentials") do
  chrome
    def button
      @driver.find_element(:id, "oauth-login")
    end
  sleep 2
  button.find_element(:xpath, "//button[@type='button']").click
  @driver.find_element(:xpath, "//input[@type='email']").send_keys("advisor@amcoonline.net")
  @driver.find_element(:id, "identifierNext").click
  sleep 3
  @driver.find_element(:xpath, "//input[@type='password']").send_keys("")
  @driver.find_element(:id, "passwordNext").click
  sleep 3
end

Then ("I should see Advisor's dashboard") do
  shipit
end
