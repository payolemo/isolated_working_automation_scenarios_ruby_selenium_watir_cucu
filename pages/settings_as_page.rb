class Settings

  include PageObject
  include DataMagic
  
  page_url 'https://stage-as.amco.me/settings'

  def validate_account_name
    account_name = self.div(:text, 'Qa Amco').text
    if account_name == 'Qa Amco'
      puts "•̀.̫•́"
    else 
      puts "ಠﭛಠ"
    end
  end

  def validate_school_name
    school_name = self.div(:text, 'Amco Soporte MX').text
    if school_name == 'Amco Soporte MX'
      puts "•̀.̫•́"
    else 
      puts "ಠﭛಠ"
    end
  end

  def change_language
    if self.li(:class, 'title').text == 'Settings'
      language_button = self.a(:class, 'modal-trigger')
      self.wait_until{language_button.present?}
      language_button.click
      language_dropdown = self.text_field(:class, 'select-dropdown')
      self.wait_until{language_dropdown.present?}
      language_dropdown.click
      sleep 1
      language_option = self.select_list(:name, 'language') #revisar porqué si se abre el dropdown pero no se selecciona el elemento dentro de el
      self.wait_until{language_option.present?}
      language_option.select_value('Español').click
    else
      language_button = self.a(:class, 'modal-trigger')
      self.wait_until{language_button.present?}
      language_button.click
      language_dropdown = self.text_field(:class, 'select-dropdown')
      self.wait_until{language_dropdown.present?}
      language_dropdown.click
      sleep 1
      language_option = self.select_list(:name, 'language')
      self.wait_until{language_option.present?}
      language_option.select_value('Inglés').click
    end
  end
end
