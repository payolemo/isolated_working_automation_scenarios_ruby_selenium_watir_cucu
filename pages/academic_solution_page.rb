class AmcoId

  include PageObject
  include DataMagic

  page_url 'https://stage-as.amco.me/'

  def enter_user(credentials, login_method)
    credentials = credentials + '_credentials'
    user_element = @browser.text_field(:id, 'user_session_username')
    @browser.wait_until {user_element.present?}
    user_element.wait_until(&:enabled?).set data_for(credentials)[login_method]
  end

  def enter_password(credentials,  pass)
    credentials = credentials + '_credentials'
    password_element = @browser.text_field(:id, 'user_session_password')
    @browser.wait_until {password_element.present?}
    password_element.wait_until(&:enabled?).set data_for(credentials)[pass]
  end

  def access_to_as
    input(:type, 'submit').click
  end

  def login_text
    content = @browser.div(:class, 'content-wrapper')
    @browser.wait_until{content.present?}
  end

  def open_help_view
    help_element = @browser.link(:href, 'https://stage-as.amco.me/help')
    @browser.wait_until{help_element.present?}
    help_element.click
  end

  def open_settings
    settings_element = @browser.link(:href, '/settings')
    @browser.wait_until{settings_element.present?}
    @browser.link(:href, '/settings').click
  end
end
