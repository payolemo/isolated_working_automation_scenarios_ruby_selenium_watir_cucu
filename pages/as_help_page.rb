class AsHelp

  include PageObject
  include DataMagic

  page_url 'stage-as.amco.me/help'

  def navigate_to_callme
    callme_element = @browser.link(:href, 'https://stage-soporte.amco.me')
    @browser.wait_until{callme_element.present?}
    callme_element.click
  end

  def validate_callme_url
    if @browser.windows.last.use.url == 'https://stage-soporte.amco.me/'
      puts "•̀.̫•́"
    else 
      puts "ಠﭛಠ"
    end
  end
end
